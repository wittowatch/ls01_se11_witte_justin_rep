/**
  *
  * Beschreibung
  *  Einf�hrung in die Programmierung mit Arrays 
  * @version 1.0 vom 19.03.2017
  * @author 
  */
import java.util.Scanner;
public class �bungArray {
  
  public static void main(String[] args) {
    
    Scanner sc = new Scanner (System.in);
    
    //Deklaration eines Arrays, 5 integer Werte
    //Name des Arrays: "intArray"
    
    int intArray[] = new int[5];
    

    
    //Speichern von Werten im Array
    //Wert 1000 an 3. Stelle des Arrays
    //Wert 500 an Index 4
    
    intArray[2] = 1000;
    intArray[4] = 500;
    
    
    
    //Deklaration + Initialisierung eines Arrays in einem Schritt
    //Name des Arrays: "doubleArray"
    // L�nge 3, mit 3 double Werten 
    
    double doubleArray[] = {1.2,1.3,1.4};
    
    
    //Der Wert des double Arrays an Indexposition 1 soll ausgegeben werden
    
    System.out.println(doubleArray[1]);
    
    
    //Nutzer soll einen Index angeben 
    //und an diesen Index einen neuen Wert speichern
    
    System.out.println("An welchen Index soll der neue Wert?");
    int index;
    index = sc.nextInt();
    
    double eingabe;
    System.out.println("Geben Sie den neuen Wert f�r index " +       " an:");
    eingabe = sc.nextDouble();
    doubleArray[index] = eingabe;
    
    
    
    
    //Alle Werte vom Array intArray sollen ausgegeben werden
    //Geben Sie zun�chst an, welche Werte Sie in der Ausgabe erwarten: 
    // null   null   1000  500  null
    
    for (int i = 0; i<=5;i++)
    {
    	System.out.println(intArray[i]);
    }
    
    
    
    
    //Der intArray soll mit neuen Werten gef�llt werden
          //1. alle Felder sollen den Wert 0 erhalten
    
    for (int i = 0; i<=6;i++)
    {
    	intArray[i]=0;
    }
    
    
    
    
    //Der intArray soll mit neuen Werten gef�llt werden
          //2. alle Felder sollen vom Nutzer einen Wert bekommen
          //nutzen Sie dieses Mal die Funktion �length�
    int ein;
    
    for (int i = 0; i<=6;i++)
    {
    System.out.println("geben sie einen wert ein:");
    ein = sc.nextInt();
    intArray[i] = ein;
    }
    
    
    
    
    //Der intArray soll mit neuen Werten gef�llt werden
           //3. Felder automatisch mit folgenden Zahlen f�llen: 10,20,30,40,50
    
    for (int i = 0; i<=10; i++)
    {
    	intArray[i] = (i+1)*10;
    	System.out.println(intArray[i]);
    }
    
    
    
    
    
    
  } // end of main
  
} // end of class arrayEinfuehrung
