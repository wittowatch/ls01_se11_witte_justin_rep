import java.util.Scanner;

public class Konsoleneingabe 
{

	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		Scanner hs = new Scanner(System.in);
		
		double zahl1;
		double zahl2;
		double ergebnis;
		char operat;
		byte operator;
		
		System.out.println("Bitte erste Zahl eingeben");
		zahl1 = hs.nextDouble();
		
		System.out.println("Bitte die zweite Zahl eingeben");
		zahl2 = hs.nextDouble();
		
		System.out.println("Operator eingeben");
		operat = hs.next().charAt(0);
		switch (operat) 
		{
			case '/' :
				ergebnis = zahl1 / zahl2;
				System.out.printf("%.2f / %.2f = %.2f", zahl1, zahl2 ,ergebnis);
			break;
			case '*' :
				ergebnis = zahl1 * zahl2;
				System.out.printf("%.2f * %.2f = %.2f", zahl1, zahl2, ergebnis);
			break;
			case '-': 
				ergebnis = zahl1 - zahl2;
				System.out.printf("%.2f - %.2f = %.2f", zahl1, zahl2 ,ergebnis);
			break;
			case '+' :
				ergebnis = zahl1 + zahl2;
				System.out.printf("%.2f + %.2f = %.2f", zahl1, zahl2 ,ergebnis);
		 }		
	  }
}
